# Einführung in Git und GitLab

Willkommen zum GitLab Workshop! Dieses Repository enthält alle erforderlichen Informationen und Ressourcen für den Workshop zur Einführung in Git und GitLab.

## Inhalte

- [Ablauf](ablauf.md): Gibt einen Überblick über den Workshop-Zeitplan und die Aktivitäten.
- [Aufgaben](aufgaben.md): Beschreibt die Aufgaben und Übungen, an denen die Teilnehmerinnen und Teilnehmer während des Workshops arbeiten werden.

## Erste Schritte

Bevor Sie in den Workshop einsteigen, empfiehlt es sich, sich mit den Grundlagen von Git und GitLab vertraut zu machen. Hier sind einige Ressourcen, um loszulegen:

- [Git-Dokumentation](https://git-scm.com/doc): Offizielle Dokumentation für Git.
- [GitLab-Grundlagen](https://docs.gitlab.com/): Offizielle Dokumentation von GitLab für den Einstieg in GitLab.

## Voraussetzungen

1. Grundkenntnisse in Git und Versionskontrollkonzepten.
2. Vertrautheit mit der Befehlszeilenschnittstelle (CLI) zur Ausführung von Git-Befehlen.
3. Zugang zu einem Computer mit einem modernen Webbrowser und Internetverbindung.
4. Jeder Teilnehmer sollte ein eigenes GitLab-Konto haben (entweder ein persönliches Konto oder Zugang zu einem gemeinsamen Konto, das für den Workshop erstellt wurde).

## Ziel

Erstellung eines kollaborativen Sommer-Reiseführers mit GitLab.

Die Daten für die Orte müssen noch als Teil der [Übung](aufgaben.md) hochgeladen werden (siehe [Issues Board](https://gitlab.com/mcdciumr/WinterOfGames/-/boards)).

Wir haben eine Pipeline eingerichtet, um eine .pdf-Datei zu kompilieren, sobald Änderungen in das Repository eingegeben werden. Am Ende des Workshops sollten wir in der Lage sein, einen vollständigen, überprüften und fehlerfreien Reiseführer herunterzuladen.

## Mitarbeit

Um zu diesem Workshop-Material beizutragen, befolgen Sie diese Schritte:

1. Klonen Sie das Repository: `git clone [Repository-URL]`
2. Erstellen Sie einen neuen Branch: `git checkout -b [Branch-Name]`
3. Führen Sie Ihre Änderungen und Ergänzungen durch.
4. Committen Sie Ihre Änderungen: `git commit -m "Ihre Commit-Nachricht"`
5. Pushen Sie Ihren Branch in das Remote-Repository: `git push origin [Branch-Name]`
6. Eröffnen Sie einen Merge Request, um Ihre Änderungen zu überprüfen und zu mergen.

## Empfohlene Software

Für eine optimale Workshop-Erfahrung empfehlen wir die Verwendung folgender Software:

- **Visual Studio Code**: Ein leistungsstarker und plattformübergreifender Code-Editor mit integrierter Git-Unterstützung. [Hier herunterladen](https://code.visualstudio.com/)
    
- **Git**: Ein verteiltes Versionskontrollsystem, das wir während des Workshops verwenden werden. Installationsanleitungen finden Sie in der offiziellen [Git-Dokumentation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
    
- **GitLab**: Eine webbasierte Plattform zur Zusammenarbeit und Versionsverwaltung von Code. Während des Workshops werden wir GitLab für das Hosting und die Zusammenarbeit an den Workshop-Aufgaben verwenden. Sie können kostenlos ein Konto auf [GitLab](https://gitlab.com/) erstellen.

## Lizenz

Dieses Workshop-Material steht unter der [MIT-Lizenz](LICENSE).
