# Sommer-Reiseführer

## Skandinavien

### Stockholm, Schweden

**Temperatur:** 20-25°C

**Outdoor-Aktivitäten:** Besuchen Sie den Schärengarten, die Insel Djurgården und das Königliche Schloss.

**Essen und Trinken:** Probieren Sie die Fleischbällchen, Hering und Preiselbeermarmelade. Genießen Sie ein lokales Bier oder Schnaps.

**Stadt Highlights:** Gamla Stan (Altstadt), Vasa-Museum und ABBA The Museum.

### Oslo, Norwegen

**Temperatur:** 16-22°C

**Outdoor-Aktivitäten:** Erkunden Sie den Oslofjord, den Vigeland-Park und die Skisprungschanze Holmenkollen.

**Essen und Trinken:** Genießen Sie norwegischen Lachs, Klippfisk und Brunost-Käse. Probieren Sie Aquavit.

**Stadt Highlights:** Wikingerschiff-Museum, Festung Akershus und die Norwegische Nationaloper & Ballett.

## Balkan

### Dubrovnik, Kroatien

**Temperatur:** 25-30°C

**Outdoor-Aktivitäten:** Spazieren Sie auf den Stadtmauern, besuchen Sie die Insel Lokrum und erkunden Sie die Elaphiten-Inseln.

**Essen und Trinken:** Probieren Sie schwarzen Risotto, Peka und lokales Olivenöl. Probieren Sie Prošek-Wein.

**Stadt Highlights:** Altstadt, Festung Lovrijenac und die Kathedrale von Dubrovnik.

### Sarajevo, Bosnien und Herzegowina

**Temperatur:** 20-30°C

**Outdoor-Aktivitäten:** Besuchen Sie den Park Vrelo Bosne, den Berg Jahorina und den Berg Trebević.

**Essen und Trinken:** Probieren Sie Ćevapi, Burek und bosnischen Kaffee.

**Stadt Highlights:** Baščaršija (Alter Basar), Gazi-Husrev-Beg-Moschee und Sarajevo-Tunnel.

## Mittelmeer

### Barcelona, Spanien

**Temperatur:** 25-30°C

**Outdoor-Aktivitäten:** Besuchen Sie den Strand, den Berg Montjuic und den Park Guell.

**Essen und Trinken:** Genießen Sie Paella, Tapas und Cava.

**Stadt Highlights:** Sagrada Familia, Gotisches Viertel und Picasso-Museum.

### Santorini, Griechenland

**Temperatur:** 26-30°C

**Outdoor-Aktivitäten:** Erkunden Sie die Strände, wandern Sie von Fira nach Oia und besuchen Sie die antiken Ruinen von Akrotiri.

**Essen und Trinken:** Probieren Sie griechischen Salat, Moussaka und Ouzo.

**Stadt Highlights:** Weiß getünchte Gebäude von Oia, Alt-Thira und das Archäologische Museum.

## Nordost-Brasilien

### Recife, Brasilien

**Temperatur:** 25-30°C

**Outdoor-Aktivitäten:** Besuchen Sie den Strand Boa Viagem, erkunden Sie die Riffe und genießen Sie die natürlichen Pools von Porto de Galinhas.

**Essen und Trinken:** Probieren Sie Acarajé, Vatapá und Caipirinha.

**Stadt Highlights:** Alt-Recife, Instituto Ricardo Brennand und die Kathedrale São Pedro dos Clérigos.

### Fortaleza, Brasilien

**Temperatur:** 28-31°C

**Outdoor-Aktivitäten:** Besuchen Sie Praia do Futuro, den Strand von Cumbuco und das Kulturzentrum Dragão do Mar.

**Essen und Trinken:** Genießen Sie Caranguejo, Baião de Dois und Cajuína.

**Stadt Highlights:** Zentralmarkt, Metropolitan-Kathedrale und das Theater José de Alencar.

## Westkanada

### Vancouver, Kanada

**Temperatur:** 18-22°C

**Outdoor-Aktivitäten:** Besuchen Sie den Stanley-Park, den Grouse Mountain und die Capilano-Hängebrücke.

**Essen und Trinken:** Probieren Sie Poutine, Lachs und kanadisches Bier.

**Stadt Highlights:** Granville Island, Gastown und das Vancouver Aquarium.

### Banff, Kanada

**Temperatur:** 15-25°C

**Outdoor-Aktivitäten:** Erkunden Sie den Banff-Nationalpark, den Lake Louise und den Moraine Lake.

**Essen und Trinken:** Genießen Sie Bison, Tourtière und kanadischen Whisky.

**Stadt Highlights:** Banff-Gondel, Bow Falls und das Banff Centre.