# Übungsaufgaben

## Aufgabe 1: Klonen eines GitLab-Projekts

- Übung: Jeder Teilnehmer klont das Projekt und erstellt einen neuen Branch. 

## Aufgabe 2: Jeder Teilnehmer weist sich selbst ein oder mehrere Issues zu.

## Aufgabe 3: Erstellen einer Merge-Anfrage

- Übung: Der Teilnehmer erstellt eine Merge-Anfrage, um sein Beitrag zum Hauptbranch hinzuzufügen. Sie geben eine
  Beschreibung ihrer Änderungen an und lassen einen anderen Teilnehmer eine Code- (oder in diesem Fall Text-)
  Überprüfung durchführen.

## Aufgabe 4: Merge-Anfragen überarbeiten und annehmen

- Übung: Die Teilnehmer überprüfen die Merge-Anfrage des Rezepts, in dem sie markiert wurden. Sie geben Feedback,
  Vorschläge oder Genehmigungen in der Diskussion zur Merge-Anfrage. Wenn Änderungen erforderlich sind, nimmt der
  ursprüngliche Teilnehmer die Änderungen in ihrem Branch vor und aktualisiert die Merge-Anfrage.

## Aufgabe 5: Erstellen von Issues

- Übung: Die Teilnehmer überprüfen die vorhandenen Reiseziele auf Fehler und erzeugen Issues, die die gefundenen Fehler
  beschreiben. Zur Behebung der Fehler werden Merge-Anfrage erzeugt, nachdem Teilnehmer sich der Issue annehmen, indem
  sie sich als Assignee zuweisen. Mit Hilfe von Issue-Templates wird garantiert, dass Bug-Berichte einheitlich
  formatiert sind und alle nötigen Informationen enthalten.

## Aufgabe 6: Lösen von Merge-Konflikten

- Übung: Die Teilnehmer führen absichtlich widersprüchliche Änderungen in derselben Datei durch. Sie versuchen dann, die
  Texte zusammenzuführen und die Konflikte mit Hilfe der Merge-Anfragenoberfläche von GitLab
  ([falls möglich](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html)) oder in einem lokalen
  Text-Editor zu lösen.
