# GitLab Workshop: Kollaboratives Arbeiten mit Git und GitLab (Weihnachtsrezeptbuch-Projekt)

## Einstieg
1. Begrüßung der Teilnehmer
2. Vorstellung der Workshop-Ziele und des Ablaufplans
3. Kurze Vorstellungsrunde oder Icebreaker-Aktivität, um die Teilnehmer miteinander vertraut zu machen

## Arbeitsphase
1. [Einführung](einfuerung-in-git-und-gitlab.md) in GitLab und Git-Grundlagen
   - Überblick über die Funktionen und Vorteile von GitLab
   - Verständnis der GitLab-Oberfläche und Navigation
   - Grundlegende Git-Befehle und -Konzepte (Commit, Branch, Merge usw.)
      - Klonen eines GitLab-Projekts
      - Änderungen vornehmen und festschreiben
      - Änderungen in das remote Repository übertragen (Push)
      - Änderungen vom remote Repository abrufen (Pull)
      - Zweige in GitLab erstellen und verwalten (Branches)
      - Merge Requests und ihre Funktion verstehen
      - Merge-Konflikte
   - Code Reviews mit Merge Requests
      - Merge Requests in GitLab erstellen und verwalten
      - Codeänderungen prüfen und kommentieren
      - Merge Requests genehmigen und zusammenführen
      - Verständnis von Git-Workflows (zentralisiert, Feature Branch, Gitflow)

2. [Aufgaben](aufgaben.md)

- Klonen des GitLab-Projekts
- Erstellen einer Markdown Datei
- Erstellen einer Merge-Request
- Überprüfen einer Merge-Request
- Lösen von Merge-Konflikten

## Abschluss
1. Abschlussdiskussion und Feedback
   - Diskussion über die Erfahrungen der Teilnehmer und das Endergebnis des Projekts
   - Sammeln von Feedback zur Verbesserung zukünftiger Workshops

2. Zusätzliche Ressourcen
   - Bereitstellung von Links zu relevanten GitLab-Dokumentationen, Tutorials oder externen Ressourcen für weiteres Lernen
