## Skandinavien

### Stockholm, Schweden

**Temperatur:** 20-25°C

**Outdoor-Aktivitäten:** Besuchen Sie den Schärengarten, die Insel Djurgården und das Königliche Schloss.

**Essen und Trinken:** Probieren Sie die Fleischbällchen, Hering und Preiselbeermarmelade. Genießen Sie ein lokales Bier oder Schnaps.

**Stadt Highlights:** Gamla Stan (Altstadt), Vasa-Museum und ABBA The Museum.

### Oslo, Norwegen

**Temperatur:** 16-22°C

**Outdoor-Aktivitäten:** Erkunden Sie den Oslofjord, den Vigeland-Park und die Skisprungschanze Holmenkollen.

**Essen und Trinken:** Genießen Sie norwegischen Lachs, Klippfisk und Brunost-Käse. Probieren Sie Aquavit.

**Stadt Highlights:** Wikingerschiff-Museum, Festung Akershus und die Norwegische Nationaloper & Ballett.