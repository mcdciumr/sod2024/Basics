## Mittelmeer

### Barcelona, Spanien

**Temperatur:** 25-30°C

**Outdoor-Aktivitäten:** Besuchen Sie den Strand, den Berg Montjuic und den Park Guell.

**Essen und Trinken:** Genießen Sie Paella, Tapas und Cava.

**Stadt Highlights:** Sagrada Familia, Gotisches Viertel und Picasso-Museum.

### Santorini, Griechenland

**Temperatur:** 26-30°C

**Outdoor-Aktivitäten:** Erkunden Sie die Strände, wandern Sie von Fira nach Oia und besuchen Sie die antiken Ruinen von Akrotiri.

**Essen und Trinken:** Probieren Sie griechischen Salat, Moussaka und Ouzo.

**Stadt Highlights:** Weiß getünchte Gebäude von Oia, Alt-Thira und das Archäologische Museum.