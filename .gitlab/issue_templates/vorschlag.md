## Name

Der Name des neuen Rezepts

## Typ

- [ ] Vorspeise
- [ ] Hauptspeise
- [ ] Dessert
- [ ] Getränk

## Link zum Rezept

Wenn bereits vorhanden, Links zu den Zutaten und der Zubereitung

/label ~Rezeptvorschlag
